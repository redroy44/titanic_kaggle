import pandas as pd
import numpy as np
import pylab as P
import csv as csv

from sklearn.preprocessing import normalize
from sklearn.ensemble import RandomForestClassifier

df = pd.read_csv('data/train.csv', header=0)

# show age distribution
#histogram = df['Age'].dropna().hist(bins=16, range=(0,80), alpha=.5)
#P.show(histogram)

# transform Sex into int female:0 and male:1
df['Gender'] = df['Sex'].map( {'female': 0, 'male': 1} ).astype(int)

# assume that is embarked value is missing its value is 'S'
df['EmbarkedFill'] = df['Embarked']
df.loc[(df.Embarked.isnull()), 'EmbarkedFill'] = 'S'

# transform origin places to ints
df['Source'] = df['EmbarkedFill'].map({'S' : 0, 'C' : 1, 'Q' : 2}).astype(int)

# fill missing Age values with mean age value
df['AgeFill'] = df['Age']
df.loc[(df.Age.isnull()), 'AgeFill'] = df['Age'].mean()

# check if place of origin influences survival rate
for i in range(0,3):
    print i, float(len(df[ (df['Survived'] == 1) & (df['Source'] == i) ])) / len(df[(df['Source'] == i)])

# Time for some feature engineering

# create family feature
df['FamilySize'] = df['SibSp'] + df['Parch']

# artificial feature from the tutorial
df['Age*Class'] = df.AgeFill * df.Pclass

# the more you pay in each class
df['Fare/Class'] = df.Fare / df.Pclass

# now drop the unused columns
df = df.drop(['PassengerId', 'Name', 'Sex', 'Ticket', 'Cabin', 'Embarked', 'Age', 'EmbarkedFill'], axis=1)

# drop any unexpected NA
df = df.dropna()

# convert dataset into numpy array
train_data = df.values
# normalize the features? (x - mean)/var
#train_data_normalized = normalize(train_data[:,1:])
train_data_normalized = train_data[:,1:]

# TEST DATA
test_df = pd.read_csv('data/test.csv', header=0)

# transform Sex into int female:0 and male:1
test_df['Gender'] = test_df['Sex'].map( {'female': 0, 'male': 1} ).astype(int)

# assume that is embarked value is missing its value is 'S'
test_df['EmbarkedFill'] = test_df['Embarked']
test_df.loc[(test_df.Embarked.isnull()), 'EmbarkedFill'] = 'S'

# transform origin places to ints
test_df['Source'] = test_df['EmbarkedFill'].map({'S' : 0, 'C' : 1, 'Q' : 2}).astype(int)

# fill missing Age values with mean age value
test_df['AgeFill'] = test_df['Age']
test_df.loc[(test_df.Age.isnull()), 'AgeFill'] = test_df['Age'].mean()

# assume that is Fare value is missing its value is mean of fare
test_df.loc[(test_df.Fare.isnull()), 'Fare'] = test_df['Fare'].mean()

# create family feature
test_df['FamilySize'] = test_df['SibSp'] + test_df['Parch']

# artificial feature from the tutorial
test_df['Age*Class'] = test_df.AgeFill * test_df.Pclass

# the more you pay in each class
test_df['Fare/Class'] = test_df.Fare / test_df.Pclass

# Collect the test data's PassengerIds before dropping it
ids = test_df['PassengerId'].values

# now drop the unused columns
test_df = test_df.drop(['PassengerId', 'Name', 'Sex', 'Ticket', 'Cabin', 'Embarked', 'Age', 'EmbarkedFill'], axis=1)

# convert dataset into numpy array
test_data = test_df.values
# normalize the features? (x - mean)/var
#test_data_normalized = normalize(test_data[:,:])
test_data_normalized = test_data


#start with a RandomForestClassifier

# Create the random forest object which will include all the parameters
# for the fit
forest = RandomForestClassifier(n_estimators = 100)

# Fit the training data to the Survived labels and create the decision trees
print 'Training...'
forest = forest.fit(train_data_normalized[0::,:],train_data[0::,0])

print 'Predicting...'
output = forest.predict(test_data_normalized).astype(int)

predictions_file = open("myfirstforest1.csv", "wb")
open_file_object = csv.writer(predictions_file)
open_file_object.writerow(["PassengerId","Survived"])
open_file_object.writerows(zip(ids, output))
predictions_file.close()
print 'Done.'
